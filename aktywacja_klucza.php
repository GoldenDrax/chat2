<?php

	session_start();

	if(isset($_GET['klucz']))
	{
		$klucz=$_GET['klucz'];
		
		require_once "dbConnect.php";
		try
			{
				$polaczenie = new mysqli($host, $user, $password, $database);
				
				if ($polaczenie->connect_error!=0) 
				{
					throw new Exception(mysqli_connect_errno());
				}
				else
				{
					$rezultat=$polaczenie->query("SELECT id FROM uzytkownicy WHERE kod_aktywacyjny='$klucz' AND czy_aktywny='nie'");	
						
					if (!$rezultat) throw new Exception ($polaczenie->error);
						
					$ilu_userow=$rezultat->num_rows;
					if ($ilu_userow==1)
					{	
						
						if($polaczenie->query("UPDATE uzytkownicy SET czy_aktywny='tak' WHERE kod_aktywacyjny='$klucz'"))
						{
							unset ($_SESSION['e_aktywowane_konto']);
							$_SESSION['aktywowane_konto']="Twoje konto zostało poprawnie aktywowane. Nastąpi przekierowanie do strony logowania.";
							header('Location: aktywowano.php');
						}
						else
						{
							throw new Exception ($polaczenie->error);
						}
					}
					else
					{
						$_SESSION['e_aktywowane_konto']='<span style="color:red">Konto nie zostało poprawnie aktywowane lub już zostało aktywowane.</span>';						
						header('Location: aktywowano.php');
					}
						
					$polaczenie->close();
				}
			}
			catch (Exception $error)
			{
				echo '<span style="color:red">Błąd serwera. Spróbuj aktywować konto później.</span>';
				echo "<br />Informacje o błędzie: ".$error;
			}
	}
?>