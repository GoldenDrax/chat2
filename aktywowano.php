<?php
	session_start();
?>
<html>
<title>
Aktywacja
</title>
<head lang="pl">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<link href='https://fonts.googleapis.com/css?family=Lato:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

<style>
body 
{
	background-color:#303030;
	color:#ffffff;
	font-family: 'Lato', sans-serif;
	margin:0;
	padding:0;
}
#dane
{
	 width:100%;
	 padding:50px;
}
.aktywowano
{
	margin-left:auto;
	margin-right:auto;
	font-size:72px;
	text-align:center;
}
</style>

</head>

<body>
	<div id="dane">
		<div class="aktywowano">
			<?php
				if(isset($_SESSION['aktywowane_konto']))
				{
					echo $_SESSION['aktywowane_konto'];
					header("refresh:7; url=logowanie.php");
					unset ($_SESSION['aktywowane_konto']);
				}
				
				if(isset($_SESSION['e_aktywowane_konto']))
				{
					echo $_SESSION['e_aktywowane_konto'];
					unset ($_SESSION['e_aktywowane_konto']);
				}
				?>
		</div>
		<center>
			
		</center>
	</div>

</body>
</html>
