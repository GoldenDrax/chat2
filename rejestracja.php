﻿<?php
	
	session_start();


	if (isset($_POST['email']))
	{	
		$flaga=true;
			
			
		//walidacja nicku
		$nick=$_POST['nick'];
		if((strlen($nick)<4)||(strlen($nick)>15))
		{
			$flaga=false;
			$_SESSION['e_nick']="Nick musi mieć od 4 do 15 znaków";
		}
		if (ctype_alnum($nick)==false)
		{
			$flaga=false;
			$_SESSION['e_nick']="Niepoprawny nick";
		}
		
			
		//walidacja emailu
		$email=$_POST['email'];
		$email_w=filter_var($email,FILTER_SANITIZE_EMAIL);
		
		if ((filter_var($email_w,FILTER_VALIDATE_EMAIL)==false) || ($email!=$email_w))
		{
			$flaga=false;
			$_SESSION['e_email']="Niepoprawny email";
		}
		
		
		//walidacja hasła
		$haslo1=$_POST['haslo1'];
		$haslo2=$_POST['haslo2'];
		if((strlen($haslo1)<8)||(strlen($haslo1)>20))
		{
			$flaga=false;
			$_SESSION['e_haslo']="Hasło musi mieć od 8 do 20 znaków";
		}
		if($haslo1!=$haslo2)
		{
			$flaga=false;
			$_SESSION['e_haslo']="Hasła nie zgadzają się";
		}
		
		//łączenie z bazą
		require_once "dbConnect.php";
		try
		{
			
			$polaczenie = new mysqli($host, $user, $password, $database);
			
			if ($polaczenie->connect_error!=0) 
			{
				throw new Exception(mysqli_connect_errno());
			}
			else
			{
				//czy email juz istnieje
				$rezultat=$polaczenie->query("SELECT id FROM uzytkownicy WHERE email='$email'");
				
				if (!$rezultat) throw new Exception ($polaczenie->error);
				
				$zwrocone_wiersze_mail=$rezultat->num_rows;
				if ($zwrocone_wiersze_mail>0)
				{
					$flaga=false;
					$_SESSION['e_email']="Ten email już istnieje";
				}
				
				//czy nick juz istnieje
				$rezultat=$polaczenie->query("SELECT id FROM uzytkownicy WHERE login='$nick'");
				
				if (!$rezultat) throw new Exception ($polaczenie->error);
				
				$zwrocone_wiersze_nick=$rezultat->num_rows;
				if ($zwrocone_wiersze_nick>0)
				{
					$flaga=false;
					$_SESSION['e_nick']="Ten nick już istnieje";
				}
				
				//dodanie rekordów do bazy
				if ($flaga==true)
				{
					$data=date('Y-m-d H:i:s');
					$kod_aktywacyjny=md5(rand(1,9999999));
					
					if($polaczenie->query ("INSERT INTO uzytkownicy VALUES (NULL,'$nick','$haslo1','$email','$data','nie','$kod_aktywacyjny')"))
					{
						
						require_once 'PHPMailerAutoload.php';
						
						$link_aktywacyjny="http://localhost/strony/czat/aktywacja_klucza.php?klucz=".$kod_aktywacyjny;
		
						require_once('class.phpmailer.php');    // dodanie klasy phpmailer
						require_once('class.smtp.php');    // dodanie klasy smtp
						$mail = new PHPMailer();    //utworzenie nowej klasy phpmailer
						$mail->CharSet = "UTF-8";
						$mail->From = "gorecki-dom@o2.pl";    //Pełny adres e-mail
						$mail->FromName = "Najlepszy czat w świecie";    //imię i nazwisko lub nazwa użyta do wysyłania wiadomości
						$mail->Host = "poczta.o2.pl";    //adres serwera SMTP wysyłającego e-mail
						$mail->Mailer = "smtp";    //do wysłania zostanie użyty serwer SMTP
						$mail->SMTPAuth = true;    //włączenie autoryzacji do serwera SMTP
						$mail->Username = "gorecki-dom@o2.pl";    //nazwa użytkownika do skrzynki e-mail
						$mail->Password = "Yhnujmikl1@";    //hasło użytkownika do skrzynki e-mail
						$mail->Port = 587; //port serwera SMTP 
						$mail->Subject = "Aktywacja konta: $nick";    //Temat wiadomości, można stosować zmienne i znaczniki HTML
						$mail->isHTML(true);
						$mail->Body = 
						'<center>
							<h1>Link aktywacyjny do czatu najlepszego w świecie</h1>
							<br /><br />
							<a href=http://localhost/strony/czat/aktywacja_klucza.php?klucz='.$kod_aktywacyjny.'>'.$link_aktywacyjny.'</a>
						</center>';    //Treść wiadomości, można stosować zmienne i znaczniki HTML 
						$mail->SMTPAutoTLS = false;   //wyłączenie TLS
						$mail->SMTPSecure = '';    // 
						$mail->AddAddress ($email,$nick);    //adres skrzynki e-mail oraz nazwa
																			//adresata, do którego trafi wiadomość
						if($mail->Send())    //sprawdzenie wysłania, jeśli wiadomość została pomyślnie wysłana
						{                      
							$_SESSION['w_aktywacyjna']="Link aktywacyjny został wysłany na konto: $email.";	//wyświetl ten komunikat
							
						}            
						else    //w przeciwnym wypadku
						{           
							$_SESSION['w_aktywacyjna']="E-mail nie mógł zostać wysłany.";    //wyświetl następujący
							
						}

					}
					else
					{
						throw new Exception ($polaczenie->error);
					}
				}
				
				$polaczenie->close();
			}
		}
		catch (Exception $error)
		{
			echo '<span style="color:red">Błąd serwera. Spróbuj zarejestrować się później.</span>';
			echo "<br />Informacje o błędzie: ".$error;
		}
	}
?>

<html>
<title>
Rejestracja
</title>
<head lang="pl">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<link rel="Stylesheet" type="text/css" href="rejestracja_css.css" />
<link rel="Stylesheet" type="text/css" href="css/fontello.css" />
<link href='https://fonts.googleapis.com/css?family=Lato:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

</head>

<body>

	<div id="container">
		<div class="header">
			<div class="logo">
					<i class="icon-chat-empty"></i>Chat najlepszy w świecie
			</div>
		</div> 
		<div id="content">
			<div class="powitanie">
				Rejestracja
			</div>
			
			<form method="post">
				<div class="formularz">
					<input type="text" name="nick" placeholder="Login" onfocus="this.placeholder='od 4 do 15 znaków'" onblur="this.placeholder='Login'"/>
						<?php
							if (isset($_SESSION['e_nick']))
							{
								echo '<div class="error">'.$_SESSION['e_nick'].'</div>';
								unset ($_SESSION['e_nick']);
							}
						?>
					<input type="password" name="haslo1" placeholder="Hasło" onfocus="this.placeholder='od 8 do 20 znaków'" onblur="this.placeholder='Hasło'"/>
						<?php
							if (isset($_SESSION['e_haslo']))
							{
								echo '<div class="error">'.$_SESSION['e_haslo'].'</div>';
								unset ($_SESSION['e_haslo']);
							}
						?>
					<input type="password" name="haslo2" placeholder="Powtórz hasło"/>
					
					<input type="text" name="email" placeholder="Email" onfocus="this.placeholder='przykład@poczta.pl'" onblur="this.placeholder='Email'"/>
						<?php
							if (isset($_SESSION['e_email']))
							{
								echo '<div class="error">'.$_SESSION['e_email'].'</div>';
								unset ($_SESSION['e_email']);
							}
						?>
				</div>
				
				<div class="przyciski">
				<center>
					<input type="submit" id="zarejestruj" name="zarejestruj" value="Zarejestruj"/>
					<div class="home">
						<a href="index.html" class="homebtn">
							<i class="icon-home"></i>
						</a>
					</div>
				</center>
				</div>
				<div style="clear:both;"></div>	
			</form>
				<div class="stopka">
						<?php
						if (isset($_SESSION['w_aktywacyjna']))
						{
							echo $_SESSION['w_aktywacyjna'];
							unset ($_SESSION['w_aktywacyjna']);
						}
						?>
				</div>
		</div>
	</div>
	
	
</body>
</html>
