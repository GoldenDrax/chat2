<?php
	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.html');
		exit();
	}
?>
<html>
<title>
Chat
</title>
<head lang="pl">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<link rel="Stylesheet" type="text/css" href="chat_css.css" />
<link rel="Stylesheet" type="text/css" href="css/fontello.css" />
<link href='https://fonts.googleapis.com/css?family=Lato:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="wyslij.js"></script>
<script type="text/javascript" src="wyswietl.js"></script>
</head>

<body>
	<div id="container">
	
		<div class="header">
			<div class="menu">
				<div class="col-md-8">
					<div class="user">
					<?php
						if (isset($_SESSION['nickname_session']))
						{
							echo "Obecnie zalogowany jako: ".$_SESSION['nickname_session'];
						}
					?>
					</div>
				</div>
				<div class="col-md-4">
					<div class="home">
						<a href="wyloguj.php" class="homebtn">
							<i class="icon-logout"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="powitanie">
			<i class="icon-chat-empty"></i>Chat internetowy
		</div>
		
		<div id="content">
			<div class="wiadomosci">
			</div>
			
			<div class="input">
				<form method="post">
					<input type="text" id="tresc" placeholder="Twoja wiadomość"/>
					<input type="submit" value="Wyślij" id="wyslij"/>
					<div style="clear:both;"></div>
				</form>
			</div>
		</div>
		
	</div>
	
</body>
</html>