﻿<?php
	session_start();
?>
<html>
<title>
Logowanie
</title>
<head lang="pl">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<link rel="Stylesheet" type="text/css" href="logowanie_css.css" />
<link rel="Stylesheet" type="text/css" href="css/fontello.css" />
<link href='https://fonts.googleapis.com/css?family=Lato:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>

<body>
	<div id="container">
		<div class="header">
			<div class="logo">
					<i class="icon-chat-empty"></i>Chat najlepszy w świecie
			</div>
		</div>
		<div id="content">
			<div class="powitanie">
				Logowanie
			</div>
			<div class="formularz">
				<form method="post" action="zaloguj.php">
					<input type="text" name="nick" placeholder="Login"/>
					<input type="password" name="pass" placeholder="Hasło"/>	
					<div class="error">
						<?php
						if(isset($_SESSION['e_logowanie']))
						{
							echo $_SESSION['e_logowanie'];
							unset ($_SESSION['e_logowanie']);
						}
						
						if(isset($_SESSION['e_czy_aktywny']))
						{
							echo $_SESSION['e_czy_aktywny'];
							unset ($_SESSION['e_czy_aktywny']);
						}
						?>
					</div>
			</div>
				<div class="przyciski">
					<center>
						<input type="submit" id="zaloguj" name="zaloguj" value="Zaloguj"/>
						<div class="home">
							<a href="index.html" class="homebtn">
								<i class="icon-home"></i>
							</a>
						</div>
					</center>
				</div>
					<div style="clear:both;"></div>	
				</form>
		</div>
	</div>
</body>
</html>
